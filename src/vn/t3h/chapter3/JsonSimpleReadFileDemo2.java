package vn.t3h.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonSimpleReadFileDemo2 {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		JSONObject khaNangObject = (JSONObject) parser.parse(new FileReader("src/vn/t3h/chapter3/kha_nang.json"));

		KhaNang khaNang = new KhaNang();
		khaNang.setNhan_vien((NhanVien)khaNangObject.get("nhan_vien"));
		
		List<NgonNgu> listOfNgonNgu = new ArrayList<>();
		JSONArray arrNgonNgu = (JSONArray) khaNangObject.get("ngon_ngu");
		for (int idx = 0; idx < arrNgonNgu.size(); idx++) {
			NgonNgu ngonNgu = new NgonNgu();
			
			JSONObject nnObject = (JSONObject) arrNgonNgu.get(idx);
			ngonNgu.setId((int)nnObject.get("id"));
			ngonNgu.setName((String)nnObject.get("name"));
			
			listOfNgonNgu.add(ngonNgu);
		}
		khaNang.setNgon_ngu(listOfNgonNgu);
		
	}

}
