package vn.t3h.chapter3;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;

public class JsonSimpleWriteFileDemo {

	public static void main(String[] args) throws IOException {
		JSONObject nvObject = new JSONObject();
		nvObject.put("id", 1);
		nvObject.put("name", "Nguyen Van A");
		
		FileWriter fw = new FileWriter("src/vn/t3h/chapter3/person.json");
		fw.write(nvObject.toJSONString());
		
		fw.flush();
		fw.close();
		System.out.println("Write file finished!");
	}

}
