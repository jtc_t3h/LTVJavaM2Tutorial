package vn.t3h.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonSimpleReadFileDemo1 {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		
		JSONObject khaNang = (JSONObject) parser.parse(new FileReader("src/vn/t3h/chapter3/kha_nang.json"));
		
		// in toan bo noi dung
		System.out.println(khaNang);
		
		// truy xuat thuoc tinh nhan_vien
		JSONObject nvObject = (JSONObject) khaNang.get("nhan_vien");
		System.out.println(nvObject);
		String namNVObject = (String) nvObject.get("name");
		System.out.println(namNVObject);	
		
		// Truy xuat thuoc tinh kha_nang
		JSONArray arrNgonNgu = (JSONArray) khaNang.get("ngon_ngu");
		for (int idx = 0; idx < arrNgonNgu.size(); idx++) {
			JSONObject nnObject = (JSONObject) arrNgonNgu.get(idx);
			System.out.println(nnObject.get("name"));
		}
	}

}
