package vn.t3h.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class GSonReadFileDemo {

	public static void main(String[] args) throws FileNotFoundException {
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader("src/vn/t3h/chapter3/kha_nang.json"));
		
		KhaNang khaNang = gson.fromJson(reader, KhaNang.class);
		
		NhanVien nv = khaNang.getNhan_vien();
		List<NgonNgu> listOfNN = khaNang.getNgon_ngu();
				
		System.out.println(khaNang.getNhan_vien().getName());
	}

}
