package vn.t3h.chapter3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Bai1 {

	public static void main(String[] args) {
		JSONParser parser = new JSONParser();
		
		try {
			JSONObject jsObject = (JSONObject) parser.parse(new BufferedReader(new InputStreamReader(new FileInputStream("src/vn/molu/json/QLCT_1.json"), "UTF-8")));
			
			// CONG_TY
			JSONArray companyObject = (JSONArray) jsObject.get("CONG_TY");
			System.out.println(((JSONObject)companyObject.get(0)).get("Ten"));
			System.out.println(((JSONObject)companyObject.get(0)).get("Dien_thoai"));
			System.out.println(((JSONObject)companyObject.get(0)).get("Mail"));
			System.out.println(((JSONObject)companyObject.get(0)).get("Dia_chi"));
						
			// DON_VI
			JSONArray listOfDepartment = (JSONArray) jsObject.get("DON_VI");
			Iterator<JSONObject> iterator = listOfDepartment.iterator();
			while (iterator.hasNext()){
				System.out.println(iterator.next());
			}
			
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
