package vn.t3h.chapter3;

import java.util.List;

public class KhaNang {
	private NhanVien nhan_vien;
	private List<NgonNgu> ngon_ngu;

	public NhanVien getNhan_vien() {
		return nhan_vien;
	}

	public void setNhan_vien(NhanVien nhan_vien) {
		this.nhan_vien = nhan_vien;
	}

	public List<NgonNgu> getNgon_ngu() {
		return ngon_ngu;
	}

	public void setNgon_ngu(List<NgonNgu> ngon_ngu) {
		this.ngon_ngu = ngon_ngu;
	}

}
