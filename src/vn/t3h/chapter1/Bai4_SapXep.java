package vn.t3h.chapter1;

import java.util.Arrays;
import java.util.List;

/**
 * 1.5. Sắp xếp danh sách chuỗi
 * 
 * @author MoonLuna
 *
 */
public class Bai4_SapXep {

	public static void main(String[] args) {

		List<String> list = Arrays.asList("adc", "aa", "acdb");

		// Chiều dài chuỗi từ ngắn nhất tới dài nhất
		list.sort((e1, e2) -> e1.length() - e2.length());
		list.forEach(e -> System.out.println(e));

		// Chiều dài chuỗi từ dài nhất tới ngắn nhất
		list.sort((e1, e2) -> e2.length() - e1.length());
		list.forEach(e -> System.out.println(e));

		// Thứ tự Alphabet
		list.sort((e1, e2) -> e1.compareTo(e2));
		list.forEach(e -> System.out.println(e));
	}

}
