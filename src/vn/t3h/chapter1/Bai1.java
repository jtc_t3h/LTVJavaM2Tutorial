package vn.t3h.chapter1;

import java.util.Arrays;
import java.util.List;

/**
 * 1.1. Xuất mảng
 * @author MoonLuna
 *
 */
public class Bai1 {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7);
		list.forEach(e -> System.out.println(e));
		
	}

}
