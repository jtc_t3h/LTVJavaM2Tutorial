package vn.t3h.chapter1;

public interface Calculator {

	public double operator(double a, double b);

}
