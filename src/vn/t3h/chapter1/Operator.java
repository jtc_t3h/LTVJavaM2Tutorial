package vn.t3h.chapter1;

public interface Operator {
	public Integer operate(Integer operand1, Integer operand2);
}
