package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_PhepTinh extends JFrame {

	private JPanel contentPane;
	private JTextField txtSoThuNhat;
	private JTextField txtSoThuHai;
	private JTextField txtKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_PhepTinh frame = new FrmBai3_PhepTinh();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_PhepTinh() {
		setTitle("Phép tính");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 166);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtSoThuNhat = new JTextField();
		txtSoThuNhat.setBounds(106, 11, 147, 20);
		contentPane.add(txtSoThuNhat);
		txtSoThuNhat.setColumns(10);
		
		JLabel lblSThNht = new JLabel("S\u1ED1 th\u1EE9 nh\u1EA5t");
		lblSThNht.setBounds(10, 14, 86, 14);
		contentPane.add(lblSThNht);
		
		JLabel lblSThHai = new JLabel("S\u1ED1 th\u1EE9 hai");
		lblSThHai.setBounds(10, 43, 86, 14);
		contentPane.add(lblSThHai);
		
		txtSoThuHai = new JTextField();
		txtSoThuHai.setColumns(10);
		txtSoThuHai.setBounds(106, 40, 147, 20);
		contentPane.add(txtSoThuHai);
		
		JLabel lblKtQu = new JLabel("K\u1EBFt qu\u1EA3");
		lblKtQu.setBounds(10, 88, 86, 14);
		contentPane.add(lblKtQu);
		
		txtKetQua = new JTextField();
		txtKetQua.setColumns(10);
		txtKetQua.setBounds(106, 85, 147, 20);
		contentPane.add(txtKetQua);
		
		JButton btnTong = new JButton("T\u1ED5ng");
		btnTong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int toanHang1 = Integer.parseInt(txtSoThuNhat.getText().trim());
				int toanHang2 = Integer.parseInt(txtSoThuHai.getText().trim());
				
				Operator operator = new Operator() {
					
					@Override
					public Integer operate(Integer operand1, Integer operand2) {
						// TODO Auto-generated method stub
						return operand1 + operand2;
					}
				};
				int tong = operator.operate(toanHang1, toanHang2);
				
				txtKetQua.setText(String.valueOf(tong));
			}
		});
		btnTong.setBounds(265, 10, 89, 23);
		contentPane.add(btnTong);
		
		JButton btnHieu = new JButton("Hi\u1EC7u");
		btnHieu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int toanHang1 = Integer.parseInt(txtSoThuNhat.getText().trim());
				int toanHang2 = Integer.parseInt(txtSoThuHai.getText().trim());
				
				// Lambda expression
				Operator operator = (operand1, operand2) -> operand1 - operand2;
				int tong = operator.operate(toanHang1, toanHang2);
				
				txtKetQua.setText(String.valueOf(tong));
			}
		});
		btnHieu.setBounds(364, 10, 89, 23);
		contentPane.add(btnHieu);
		
		JButton btnTich = new JButton("Tích");
		btnTich.setBounds(265, 39, 89, 23);
		contentPane.add(btnTich);
		
		JButton btnThuong = new JButton("Thương");
		btnThuong.setBounds(364, 39, 89, 23);
		contentPane.add(btnThuong);
	}
}
