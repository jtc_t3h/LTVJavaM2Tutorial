package vn.t3h.chapter1;

import java.util.Arrays;
import java.util.List;

public class Bai2_TinhTien {

	public static void main(String[] args) {

		int donGia = 100000;
		List<Integer> list = Arrays.asList(1,3,5,7,9,20);
		
		list.forEach(e -> System.out.println("Thành tiền = " + (donGia * e)));
		
	}

}
