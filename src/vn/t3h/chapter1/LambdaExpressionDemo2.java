package vn.t3h.chapter1;

public class LambdaExpressionDemo2 {

	public static void main(String[] args) {

//		Calculator cal = new Calculator() {
//			
//			@Override
//			public double subtract(double a, double b) {
//				return a - b;
//			}
//			
//			@Override
//			public double multiple(double a, double b) {
//				return a * b;
//			}
//			
//			@Override
//			public double divide(double a, double b) {
//				return a/b;
//			}
//			
//			@Override
//			public double add(double a, double b) {
//				return a + b;
//			}
//		};
		
		Calculator cal1 = (a,b) -> a + b;
		System.out.println(cal1.operator(5, 6));
		
		Calculator cal2 = (a,b) -> a - b;
		System.out.println(cal2.operator(5, 6));
		
		Calculator cal3 = (a,b) -> a * b;
		System.out.println(cal3.operator(5, 6));
		
		Calculator cal4 = (a,b) -> a / b;
		System.out.println(cal4.operator(5, 6));
	}

}
