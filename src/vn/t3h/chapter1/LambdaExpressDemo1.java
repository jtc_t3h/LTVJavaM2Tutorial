package vn.t3h.chapter1;

import java.util.ArrayList;
import java.util.List;

public class LambdaExpressDemo1 {

	public static void main(String[] args) {

		List<Student> list = new ArrayList<>();
		list.add(new Student("La Thi Thuy Kieu"));
		list.add(new Student("Nguyen Cao Tri"));
		
		list.forEach( (Student student) -> System.out.println(student.getName() + ", "));
		
		list.sort((student1, student2) -> student2.getName().compareTo(student1.getName()));
		System.out.println("Danh sach sau khi sap xep giam");
		list.forEach((Student student) -> System.out.println(student.getName()));
		
		
	}

}
