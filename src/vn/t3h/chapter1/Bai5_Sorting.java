package vn.t3h.chapter1;

import java.util.Arrays;
import java.util.Comparator;

public class Bai5_Sorting {

	public static void main(String[] args) {
		String arrStr[] = {"ab", "adc", "abde"};
		
		System.out.println("Sap xep theo chuoi tu ngan nhat -> dai nhat");
		Comparator<String> com = (s1, s2) -> s1.length() - s2.length();
		
		Arrays.sort(arrStr, com);
		for (String s: arrStr) {
			System.out.println(s);
		}
		
		System.out.println("Sap xep theo chuoi tu dai nhat -> ngan nhat");
		Arrays.sort(arrStr, (s1, s2) -> s2.length() - s1.length());
		for (String s: arrStr) {
			System.out.println(s);
		}
		
		System.out.println("Sap xep theo alphabet");
		Arrays.sort(arrStr, (s1, s2) -> s1.compareTo(s2));
		for (String s: arrStr) {
			System.out.println(s);
		}
	}

}
