package vn.t3h.chapter7;

public class ConcreteProductA implements Product {

	@Override
	public void create() {
		System.out.println("ConcreteProductA class: create");
	}
}
