package vn.t3h.chapter7;

public class ConcreteObserverB extends Observer {

	public ConcreteObserverB() {
	}

	public ConcreteObserverB(Subject subject) {
		this.subject = subject;
		this.subject.registrationObserver(this);
	}

	@Override
	public void update() {
		System.out.println("ConcreteObserverB class:: update");
	}

	@Override
	public void updateState() {
		System.out.println("ConcreteObserverB class:: update " + subject.getState());

	}

}
