package vn.t3h.chapter7;

public class FactoryMethodTest {

	public static void main(String[] args) {
		Creator creator = new ConcreteCreatorA();
		Product product = creator.factoryMethod();
		product.create();
		
		Creator2 creator2 = new Creator2();
		Product product2 = creator2.factoryMethod(2);
		product2.create();
				
	}

}
