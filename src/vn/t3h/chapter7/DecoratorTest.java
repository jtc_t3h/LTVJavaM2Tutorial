package vn.t3h.chapter7;

public class DecoratorTest {

	public static void main(String[] args) {
		ConcreteDecoratorB component = new ConcreteDecoratorB(new ConcreteComponent());
		component.operation();
		component.addedBehavior();
	}

}
