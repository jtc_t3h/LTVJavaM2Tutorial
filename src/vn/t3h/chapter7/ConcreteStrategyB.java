package vn.t3h.chapter7;

public class ConcreteStrategyB implements Strategy {

	@Override
	public void execute() {
		System.out.println("ConcreteStrategyB class:: execute.");
	}

}
