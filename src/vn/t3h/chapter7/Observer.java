package vn.t3h.chapter7;

public abstract class Observer {

	protected Subject subject;
	public abstract void update();
	public abstract void updateState();
}
