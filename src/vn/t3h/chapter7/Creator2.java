package vn.t3h.chapter7;

public class Creator2 {

	public Product factoryMethod(int type) {
		if (type == 1) {
			return new ConcreteProductA();
		} else if (type == 2) {
			return new ConcreteProductB();
		}
		return null;
	}
}
