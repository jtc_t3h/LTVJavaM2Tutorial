package vn.t3h.chapter7;

import java.util.ArrayList;
import java.util.List;

public class Subject {

	private List<Observer> observers = new ArrayList<>();
	private int state;
	
	public void registrationObserver(Observer observer) {
		observers.add(observer);
	}
	
	public void unRegistrationObserver(Observer observer) {
		observers.remove(observer);
	}
	
	public void notifyObservers() {
		for (Observer observer: observers) {
			if (observer.subject == null) {
				observer.update();
			} else {
				observer.updateState();
			}
		}
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		notifyObservers();
	}
	
	
}
