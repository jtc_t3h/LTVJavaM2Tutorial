package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class FrmBai1_ThemUser extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JLabel lblPassword;
	private JTextField txtPassword;
	private JLabel lblEmail;
	private JTextField txtEmail;
	private JLabel lblSt;
	private JTextField txtSoDT;
	private JLabel lblGiiTnh;
	private JRadioButton rdbNam;
	private JRadioButton rdbNu;
	private JRadioButton rdbKhac;
	private JLabel lblaCh;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_ThemUser frame = new FrmBai1_ThemUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_ThemUser() {
		setTitle("Thêm user vào hệ thống");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 316);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 11, 66, 14);
		contentPane.add(lblUsername);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(85, 8, 179, 20);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 39, 66, 14);
		contentPane.add(lblPassword);
		
		txtPassword = new JTextField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(85, 36, 179, 20);
		contentPane.add(txtPassword);
		
		lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 67, 66, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(85, 64, 179, 20);
		contentPane.add(txtEmail);
		
		lblSt = new JLabel("Số ĐT");
		lblSt.setBounds(10, 96, 66, 14);
		contentPane.add(lblSt);
		
		txtSoDT = new JTextField();
		txtSoDT.setColumns(10);
		txtSoDT.setBounds(85, 93, 179, 20);
		contentPane.add(txtSoDT);
		
		lblGiiTnh = new JLabel("Giới tính");
		lblGiiTnh.setBounds(10, 124, 66, 14);
		contentPane.add(lblGiiTnh);
		
		rdbNam = new JRadioButton("Nam");
		rdbNam.setBounds(85, 120, 56, 23);
		contentPane.add(rdbNam);
		
		rdbNu = new JRadioButton("Nữ");
		rdbNu.setBounds(151, 120, 44, 23);
		contentPane.add(rdbNu);
		
		rdbKhac = new JRadioButton("Khác");
		rdbKhac.setBounds(208, 120, 56, 23);
		contentPane.add(rdbKhac);
		
		lblaCh = new JLabel("Địa chỉ");
		lblaCh.setBounds(10, 149, 66, 14);
		contentPane.add(lblaCh);
		
		JTextArea taDiaChi = new JTextArea();
		taDiaChi.setBounds(85, 144, 179, 83);
		contentPane.add(taDiaChi);
		
		JButton btnThem = new JButton("Thêm");
		btnThem.setBounds(84, 243, 89, 23);
		contentPane.add(btnThem);
	}
}
