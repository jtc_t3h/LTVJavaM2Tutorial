package vn.t3h.chapter7;

public class ConcreteCreatorA implements Creator {

	@Override
	public Product factoryMethod() {
		return new ConcreteProductA();
	}

}
