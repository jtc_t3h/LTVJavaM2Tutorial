package vn.t3h.chapter7;

public class Proxy implements Service {

	private RealService realService;
	private Service service;
	
	@Override
	public void defaultMethod() {
		if (realService == null) {
			realService = new RealService();
		}
		realService.defaultMethod();
	}
	
	public void defaultMethod2(int type) {
		if (type == 2) {
			if (!(service instanceof RealService2)) {
				service = new RealService2();
			}
		}
		service.defaultMethod();
	}

}
