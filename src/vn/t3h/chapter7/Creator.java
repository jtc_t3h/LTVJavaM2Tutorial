package vn.t3h.chapter7;

public interface Creator {
	
	public Product factoryMethod();
}
