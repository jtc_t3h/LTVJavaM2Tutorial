package vn.t3h.chapter7;

public class ConcreteDecoratorA extends Decorator {

	private int addState;
	
	public ConcreteDecoratorA(Component component) {
		this.component = component;
	}

	@Override
	public void operation() {
		component.operation();
	}

	public int getAddState() {
		return addState;
	}

	public void setAddState(int addState) {
		this.addState = addState;
	}
	
	
}
