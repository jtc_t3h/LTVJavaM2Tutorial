package vn.t3h.chapter7;

public class ObserverTest {

	public static void main(String[] args) {
		Subject subject = new Subject();
		subject.registrationObserver(new ConcreteObserverA());
		subject.registrationObserver(new ConcreteObserverB());
		
		subject.setState(10);
		
		Subject subject2 = new Subject();
		new ConcreteObserverA(subject2);
		new ConcreteObserverB(subject2);
//		subject2.registrationObserver(new ConcreteObserverA(subject2));
//		subject2.registrationObserver(new ConcreteObserverB(subject2));
		subject2.setState(20);
		
		
	}

}
