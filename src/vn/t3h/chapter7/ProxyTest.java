package vn.t3h.chapter7;

public class ProxyTest {

	public static void main(String[] args) {
		Service service = new RealService();
		service.defaultMethod();
		
		Service service2 = new Proxy();
		service2.defaultMethod();
		
		Proxy proxy = (Proxy) service2;
		proxy.defaultMethod2(2);
	}

}
