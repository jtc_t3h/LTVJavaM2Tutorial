package vn.t3h.chapter7;

public class ConcreteDecoratorB extends Decorator {

	public ConcreteDecoratorB(Component component) {
		this.component = component;
	}

	@Override
	public void operation() {
		component.operation();
	}
	
	public void addedBehavior() {
		System.out.println("ConcreteDecoratorB class: addedBehavior");
		
	}
}
