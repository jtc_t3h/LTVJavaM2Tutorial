package vn.t3h.chapter7;

public class SingletonTest {

	public static void main(String[] args) {
		Singleton singleton = Singleton.getInstance();
		
		System.out.println(singleton.addCount());
		System.out.println(singleton.addCount());
		
	}

}
