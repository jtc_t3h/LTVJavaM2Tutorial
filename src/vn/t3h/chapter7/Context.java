package vn.t3h.chapter7;

public class Context {

	private Strategy strategy;
	
	public Context(Strategy strategy) {
		this.strategy = strategy;
	}

	public void execution() {
		strategy.execute();
	}
}
