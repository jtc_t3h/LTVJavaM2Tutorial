package vn.t3h.chapter7;

public class ConcreteObserverA extends Observer{

	public ConcreteObserverA() {}
	
	public ConcreteObserverA(Subject subject) {
		this.subject = subject;
		this.subject.registrationObserver(this);
	}
	
	@Override
	public void update() {
		System.out.println("ConcreteObserverA class:: update");
	}

	@Override
	public void updateState() {
		System.out.println("ConcreteObserverA class:: update " + subject.getState());
	}

}
