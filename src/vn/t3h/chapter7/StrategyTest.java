package vn.t3h.chapter7;

public class StrategyTest {

	public static void main(String[] args) {
		Context context = new Context(new ConcreteStrategyA());
		context.execution();
		
		context = new Context(new ConcreteStrategyB());
		context.execution();
	}
}
