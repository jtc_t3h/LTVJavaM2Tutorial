package vn.t3h.chapter7;

public class ConcreteCreatorB implements Creator {

	@Override
	public Product factoryMethod() {
		return new ConcreteProductB();
	}

}
