package vn.t3h.chapter7;

public class ConcreteStrategyA implements Strategy{

	@Override
	public void execute() {
		System.out.println("ConcreteStrategyA class:: execute.");
	}

}
