package vn.t3h.chapter7;

public class ConcreteProductB implements Product {

	@Override
	public void create() {
		System.out.println("ConcreteProductB class: create");
	}

}
