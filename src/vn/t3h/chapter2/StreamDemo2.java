package vn.t3h.chapter2;

import java.util.stream.Stream;

public class StreamDemo2 {

	public static void main(String[] args) {
		Stream <String> stream = Stream.of("hello", "stream", "happy", "welcome", "goodbye");
		
		stream.peek(e -> System.out.println("element = " + e)).filter(e -> e.length() >= 6).forEach(e -> System.out.println(e));
		
//		long count = stream.peek(e -> System.out.println("element = " + e)).filter(e -> e.length() >= 6).count();
//		System.out.println(count);
	}

}
