package vn.t3h.chapter2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamDemo1 {

	public static void main(String[] args) {
//		Stream<String> stream = Stream.of("hello", "stream", "happy", "welcome", "goodbye");
//		Stream<String> stream = Stream.of(new String[]{"hello", "stream", "happy", "welcome", "goodbye"});
		
//		Stream<String> stream = Arrays.stream(new String[]{"hello", "stream", "happy", "welcome", "goodbye"});
		
		List<String> list = new ArrayList<>();
		list.add("hello");
		list.add("stream");
		list.add("happy");
		list.add("welcome");
		list.add("goodbye");
		
//		Stream<String> stream = list.stream();
		
		// Java 8
//		stream.forEach(e -> System.out.println(e));
		
		// Previous Java 8
//		Consumer<String> consumer = new Consumer<String>() {
//
//			@Override
//			public void accept(String t) {
//				System.out.println(t);				
//			}
//		};
//		stream.forEach(consumer);
		
		list.stream().map(e -> e + " happy").forEach(e -> System.out.println(e));
		
		list.stream().peek(e -> System.out.println(e));
		
		list.stream().filter(e -> e.startsWith("h")).map(e -> e + " happy").forEach(e -> System.out.println(e));
		
		List<String> list2 = list.stream().collect(Collectors.toList());
	}

}
