package vn.t3h.chapter2;

import java.util.ArrayList;
import java.util.List;

public class Bai3 {
	public static void main(String[] args) {
		List<Student> listOfStudent = new ArrayList<Student>();
		listOfStudent.add(new Student("Tuyen", "Le", 16, 8.0, 7.0));
		listOfStudent.add(new Student("Dien", "Nguyen", 17, 5.0, 6.0));
		listOfStudent.add(new Student("Thuy", "Nguyen", 18, 8.0, 5.0));
		listOfStudent.add(new Student("Hoang", "Ta", 19, 4.0, 7.0));
		listOfStudent.add(new Student("Thuong", "Pham", 16, 4.0, 6.0));
		listOfStudent.add(new Student("Duan", "Le", 17, 9.0, 7.0));
		listOfStudent.add(new Student("Kieu", "La", 18, 3.5, 4.0));
		listOfStudent.add(new Student("Truc", "Do", 20, 8.0, 4.0));
		listOfStudent.add(new Student("Tri", "Nguyen", 21, 8.0, 9.0));
		listOfStudent.add(new Student("Vang", "Nguyen", 22, 5.0, 6.0));

		listOfStudent.stream().forEach(student -> System.out.println(student));

		Long numOfStudentWithAgeGreate18 = listOfStudent.stream().filter(student -> student.getAge() >= 18).count();
		System.out.println("Number of student have old >= 18: " + numOfStudentWithAgeGreate18);

		Long numOfStudentWithFirstNameBeginH = listOfStudent.stream()
				.filter(student -> student.getFirstName().startsWith("H")).count();
		System.out.println("Number of student have FirtName start 'H': " + numOfStudentWithFirstNameBeginH);

		listOfStudent.stream().filter(student -> student.getFirstName().startsWith("H"))
				.forEach(student -> System.out.println(student));

		double maxOfAvg = listOfStudent.stream().max((s1, s2) -> s1.getAge() - s2.getAge()).get().avg();
		System.out.println("Highest Avg Mark in List: " + maxOfAvg);

		double minOfAvg = listOfStudent.stream().max((s1, s2) -> s2.getAge() - s1.getAge()).get().avg();
		System.out.println("Lowest Avg Mark in List: " + minOfAvg);

		double sumOfAvg = listOfStudent.stream().mapToDouble(s1 -> s1.avg()).sum();
		System.out.println("Sum of all Avg Mark: " + sumOfAvg);

		listOfStudent.stream().filter(student -> student.avg() >= 8).forEach(student -> System.out.println(student));
	}
}
