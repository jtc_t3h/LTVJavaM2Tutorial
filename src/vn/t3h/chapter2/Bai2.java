package vn.t3h.chapter2;

import java.util.Arrays;
import java.util.stream.Stream;

public class Bai2 {
	public static void main(String[] args) {
		Integer[] arrInt = { 1, 2, 3, 4, 5 };
		Arrays.stream(arrInt).forEach(e -> System.out.println(e));

		Stream<Integer> stream2 = Arrays.stream(arrInt).map(e -> e * e);
		Integer[] arrInt2 = stream2.toArray(length -> new Integer[length]);

		Arrays.stream(arrInt2).forEach(e -> System.out.println(e));
		System.out.println(Arrays.stream(arrInt2).max((n1, n2) -> n1.compareTo(n2)).get());
		System.out.println(Arrays.stream(arrInt2).min((n1, n2) -> n1.compareTo(n2)).get());
		System.out.println(Arrays.stream(arrInt2).mapToInt(i -> i).sum());
		System.out.println(Arrays.stream(arrInt2).mapToInt(i -> i).average().getAsDouble());

		Arrays.stream(arrInt2).filter(e -> isPrime(e)).forEach(e -> System.out.println(e));
		Arrays.stream(arrInt2).filter(e -> isSquareNumber(e)).forEach(e -> System.out.println(e));
	}

	public static boolean isPrime(int number) {
		for (int i = 2; i <= number / 2; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static boolean isSquareNumber(int number) {
		if (number == Math.sqrt(number) * Math.sqrt(number)) {
			return true;
		}
		return false;
	}
}
