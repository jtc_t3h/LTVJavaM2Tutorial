package vn.t3h.chapter2;

import java.util.Arrays;
import java.util.stream.Stream;

public class Bai1 {

	public static void main(String[] args) {
		// Mảng 1
		String[] arrStr = {"hello", "happy", "welcome", " ", "greeting"};
		
		Arrays.stream(arrStr).forEach((e) -> System.out.println(e));
		System.out.println(Arrays.stream(arrStr).filter(s -> s.trim().isEmpty()).count());
		System.out.println(Arrays.stream(arrStr).filter(s -> s.length() > 5).count());
		System.out.println(Arrays.stream(arrStr).filter(s -> 'w' == s.charAt(0)).count());
		System.out.println(Arrays.stream(arrStr).filter(s -> "happy".equals(s)).count());
		
		Stream<String> stream2 = Arrays.stream(arrStr).filter(s -> !s.trim().isEmpty());
		stream2.forEach((e) -> System.out.println(e));
		
		// Mang 3 tạo ra từ mảng 1
		Stream<String> stream3 = Arrays.stream(arrStr).filter(s -> s.length() >= 3 && s.length() <= 6);
		String[] arrSt3 = stream3.toArray(length -> new String[length]);
		
//		Arrays.stream(arrSt3).limit(arrSt3.length - 1).forEach(e -> System.out.print(e + ","));
//		Arrays.stream(arrSt3).skip(arrSt3.length - 1).forEach(e -> System.out.print(e));
		
		System.out.println(Arrays.stream(arrSt3).reduce((acc, item) -> acc + ", " + item).get());
		
//		System.out.println(Arrays.stream(arrStr).filter(s -> !s.trim().isEmpty()).count());
//		List<String> arrStr2 = Arrays.stream(arrStr).filter(s -> !s.trim().isEmpty()).collect(Collectors.toList());
//		arrStr2.stream().forEach((e) -> System.out.println(e));
	}
}
