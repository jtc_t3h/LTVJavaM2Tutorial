package vn.t3h.chapter2;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamDemo3 {

	public static void main(String[] args) {
		String[] arrString = new String[] {"Hello", "Supplier", "Function", "Interface"};
		
		Supplier<Stream<String>> supplier = () -> Arrays.stream(arrString);
		
		supplier.get().forEach(e -> System.out.println(e));
		long count = supplier.get().filter(e -> e.contains("e")).count();
		System.out.println(count);
		
//		Stream<String> stream = supplier.get();
//		
//		stream.forEach(e -> System.out.println(e));
//		long count = stream.filter(e -> e.contains("e")).count();
//		System.out.println(count);
	}

}
