package vn.t3h.chapter4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadXMLFileDemo2 {

	public static void main(String[] args) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document document = docBuilder.parse(new File("src/vn/t3h/chapter4/book_store.xml"));
			
			NodeList titleNodeList = document.getElementsByTagName("title");
			for (int idx = 0; idx < titleNodeList.getLength(); idx++) {
				System.out.println(titleNodeList.item(idx).getTextContent());
			}
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		

	}

}
