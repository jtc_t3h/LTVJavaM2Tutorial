package vn.t3h.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class FrmBai1_Contact extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDTDD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_Contact frame = new FrmBai1_Contact();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_Contact() {
		setTitle("Thêm mới liên hệ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 570, 263);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHTn = new JLabel("Họ tên");
		lblHTn.setBounds(10, 11, 77, 14);
		contentPane.add(lblHTn);
		
		JLabel lbltd = new JLabel("ĐTDĐ");
		lbltd.setBounds(10, 36, 46, 14);
		contentPane.add(lbltd);
		
		JLabel lblHnhnh = new JLabel("Hình ảnh");
		lblHnhnh.setBounds(10, 61, 59, 14);
		contentPane.add(lblHnhnh);
		
		JTextField txtHinhAnh = new JTextField();
		txtHinhAnh.setBounds(79, 58, 216, 20);
		contentPane.add(txtHinhAnh);
		txtHinhAnh.setColumns(10);
		
		JLabel lblHinhAnh = new JLabel("");
		lblHinhAnh.setBounds(378, 11, 166, 201);
		contentPane.add(lblHinhAnh);
		
		JButton btnBrowser = new JButton("...");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fcs = new JFileChooser("src/vn/t3h/chapter4");
				fcs.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fcs.setMultiSelectionEnabled(true);
				int returnVal = fcs.showOpenDialog(null);
				
				for (File file : fcs.getSelectedFiles()) {
					System.out.println(file.getAbsolutePath());
				}
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File fileSelected = fcs.getSelectedFile();
					
					txtHinhAnh.setText(fileSelected.getAbsolutePath());
					lblHinhAnh.setIcon(new ImageIcon(fileSelected.getAbsolutePath()));
				}
			}
		});
		btnBrowser.setBounds(301, 57, 46, 23);
		contentPane.add(btnBrowser);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(79, 8, 216, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		txtDTDD = new JTextField();
		txtDTDD.setBounds(79, 33, 216, 20);
		contentPane.add(txtDTDD);
		txtDTDD.setColumns(10);
		
		JButton btnThemMoi = new JButton("Thêm mới");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String hoTen = txtHoTen.getText().trim();
				String dtdd = txtDTDD.getText().trim();
				String hinhAnh = new File(txtHinhAnh.getText().trim()).getName();
				
				try {
					DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
					Document doc = docBuilder.newDocument();
					
					// lienhes element (root)
					Element rootElement = doc.createElement("lienhes");
					doc.appendChild(rootElement);
					
					// lienhe element
					Element lienHeElement = doc.createElement("lienhe");
					rootElement.appendChild(lienHeElement);
					
					// hoten element
					Element hoTenElement = doc.createElement("hoten");
					Text hoTenText = doc.createTextNode(hoTen);
					hoTenElement.appendChild(hoTenText);
					lienHeElement.appendChild(hoTenElement);
					
					// dtdd element
					Element dtddElement = doc.createElement("dtdd");
					Text dtddText = doc.createTextNode(dtdd);
					dtddElement.appendChild(dtddText);
					lienHeElement.appendChild(dtddElement);
					
					// hinhanh element
					Element hinhanhElement = doc.createElement("hinhanh");
					Text hinhanhText = doc.createTextNode(hinhAnh);
					hinhanhElement.appendChild(hinhanhText);
					lienHeElement.appendChild(hinhanhElement);
					
					// write the content into xml file
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					DOMSource source = new DOMSource(doc);
					StreamResult result = new StreamResult(new File("src/vn/t3h/chapter4/lienhe.xml"));
					transformer.transform(source, result);
					
					JOptionPane.showMessageDialog(rootPane, "Đã thêm liên hệ");
					
				} catch (ParserConfigurationException | TransformerException e) {
					System.out.println(e.getMessage());
					JOptionPane.showMessageDialog(rootPane, "Không thể thêm liên hệ");
				}
			}
		});
		btnThemMoi.setBounds(79, 187, 102, 23);
		contentPane.add(btnThemMoi);
	}

}
