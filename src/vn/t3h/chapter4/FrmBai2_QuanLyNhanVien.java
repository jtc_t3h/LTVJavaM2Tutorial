package vn.t3h.chapter4;

import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class FrmBai2_QuanLyNhanVien extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JComboBox comboBox;
	
	private List<Bai2_NhanVien> listOfNhanVien = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_QuanLyNhanVien frame = new FrmBai2_QuanLyNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_QuanLyNhanVien() {
		setTitle("Quản lý nhân viên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 677, 332);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 55, 641, 227);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "H\u1ECD t\u00EAn", "Gi\u1EDBi t\u00EDnh", "Ng\u00E0y sinh", "M\u1EE9c l\u01B0\u01A1ng", "\u0110\u1ECBa ch\u1EC9"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(43);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		table.getColumnModel().getColumn(3).setPreferredWidth(106);
		table.getColumnModel().getColumn(4).setPreferredWidth(80);
		table.getColumnModel().getColumn(5).setPreferredWidth(250);
		scrollPane.setViewportView(table);
		
		JLabel lblnV = new JLabel("Đơn vị");
		lblnV.setBounds(148, 23, 46, 14);
		contentPane.add(lblnV);
		
		comboBox = new JComboBox();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Bai2_DonVi selectedDonVi = (Bai2_DonVi) comboBox.getSelectedItem();
				
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				for (Bai2_NhanVien nhanVien: listOfNhanVien){
					if (nhanVien.getIdDonVi() == selectedDonVi.getId()){
						model.addRow(new Object[] {nhanVien.getId(), nhanVien.getHoTen(), nhanVien.getGioiTinh(), nhanVien.getNgaySinh(),
								nhanVien.getMucLuong(), nhanVien.getDiaChi(), nhanVien.getIdDonVi()});
					}
				}
				
			}
		});
		comboBox.setBounds(210, 20, 246, 20);
		contentPane.add(comboBox);
		
		initDataOfCombobox();
		loadListOfNhanVien();
	}

	private void loadListOfNhanVien() {
		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File("src/vn/t3h/chapter4/nhan_vien.xml"));
			
			NodeList nhanVienNodeList = doc.getElementsByTagName("nhan_vien");
			for (int idx = 0; idx < nhanVienNodeList.getLength(); idx++){
				Node nhanVienNode = nhanVienNodeList.item(idx);
				
				Bai2_NhanVien nhanVien = new Bai2_NhanVien();
				NodeList propNodeList = nhanVienNode.getChildNodes();
				for (int index = 0; index < propNodeList.getLength(); index++){
					Node propNode = propNodeList.item(index);
					if ("id".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setId(Long.parseLong(propNode.getTextContent()));
					} else if ("ho_ten".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setHoTen(propNode.getTextContent());
					} else if ("gioi_tinh".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setGioiTinh(propNode.getTextContent());
					} else if ("ngay_sinh".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setNgaySinh(propNode.getTextContent());
					} else if ("muc_luong".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setMucLuong(Long.parseLong(propNode.getTextContent()));
					} else if ("dia_chi".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setDiaChi(propNode.getTextContent());
					} else if ("id_don_vi".equalsIgnoreCase(propNode.getNodeName())){
						nhanVien.setIdDonVi(Long.parseLong(propNode.getTextContent()));
					}
				}
				listOfNhanVien.add(nhanVien);
			}
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		} 
	}

	private void initDataOfCombobox() {
		try {
			List<Bai2_DonVi> list = new ArrayList<Bai2_DonVi>();
			list.add(new Bai2_DonVi(-1, "--- Chọn ---"));
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File("src/vn/t3h/chapter4/don_vi.xml"));
			
			NodeList listOfDonVi = doc.getElementsByTagName("don_vi");
			for (int idx = 0; idx < listOfDonVi.getLength(); idx++){
				Node donviNode = listOfDonVi.item(idx);
				NodeList nodeList = donviNode.getChildNodes();
				
				Bai2_DonVi donVi = new Bai2_DonVi();
				for (int idx2 = 0; idx2 < nodeList.getLength(); idx2++) {
					Node node = nodeList.item(idx2);
					if ("id".equalsIgnoreCase(node.getNodeName())){
						donVi.setId(Long.parseLong(node.getTextContent().trim()));
					} else if ("ten".equalsIgnoreCase(node.getNodeName())) {
						donVi.setTen(node.getTextContent());
					}
				}
				list.add(donVi);
			}
			
			Bai2_DonVi[] arrDonVi = list.toArray(new Bai2_DonVi[list.size()]);
			comboBox.setModel(new DefaultComboBoxModel<>(arrDonVi));
		
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
	}
}
