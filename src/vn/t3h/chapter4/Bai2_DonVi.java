package vn.t3h.chapter4;

public class Bai2_DonVi {

	private long id;
	private String ten;

	public Bai2_DonVi(){
		
	}
	
	public Bai2_DonVi(long id, String ten) {
		this.id = id;
		this.ten = ten;
	}

	public long getId() {
		return id;
	}

	public String getTen() {
		return ten;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}
	
	@Override
	public String toString() {
		return ten;
	}
}
