package vn.t3h.chapter4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadXMLFileDemo {

	public static void main(String[] args) {
		
		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document document = docBuilder.parse(new File("src/vn/t3h/chapter4/book_store.xml"));
			
			// bookstore
			Element bookStoreElement = document.getDocumentElement();
			System.out.println(bookStoreElement.getNodeName());
			System.out.println(bookStoreElement.getNodeValue());
			System.out.println(bookStoreElement.getTextContent());
			
			// book
			NodeList bookNodeList = bookStoreElement.getChildNodes();
		    for (int index = 0; index < bookNodeList.getLength(); index++) {
		    	Node node = bookNodeList.item(index);
		    	if ("book".equalsIgnoreCase(node.getNodeName())) {
		    		Node bookNode = node;
		    		
		    		System.out.println(bookNode.getAttributes().item(0).getNodeValue());
		    	}
		    }
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
