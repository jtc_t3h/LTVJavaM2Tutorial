package vn.t3h.chapter4;

public class Bai2_NhanVien {
	private long id;
	private String hoTen;
	private String gioiTinh;
	private String ngaySinh;
	private long mucLuong;
	private String diaChi;
	private long idDonVi;

	public long getId() {
		return id;
	}

	public String getHoTen() {
		return hoTen;
	}

	public String getGioiTinh() {
		return gioiTinh;
	}

	public String getNgaySinh() {
		return ngaySinh;
	}

	public long getMucLuong() {
		return mucLuong;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public long getIdDonVi() {
		return idDonVi;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public void setMucLuong(long mucLuong) {
		this.mucLuong = mucLuong;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public void setIdDonVi(long idDonVi) {
		this.idDonVi = idDonVi;
	}

}
