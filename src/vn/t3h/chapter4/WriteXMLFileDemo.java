package vn.t3h.chapter4;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class WriteXMLFileDemo {

	public static void main(String[] args) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document document = docBuilder.newDocument();

		// SinhViens (root)
		Element sinhViens = document.createElement("SinhViens");
		document.appendChild(sinhViens);
		
		// SinhVien
		Element sinhVien = document.createElement("SinhVien");
		sinhVien.setAttribute("class", "1");
		sinhViens.appendChild(sinhVien);
		
		// hoten
		Element hoTen = document.createElement("hoten");
		Text hoTenTextNode = document.createTextNode("Trần Văn An");
		hoTen.appendChild(hoTenTextNode);
		sinhVien.appendChild(hoTen);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File("src/vn/t3h/chapter4/sinh_vien.xml"));
        transformer.transform(source, result);
        System.out.println("File saved!");

	}

}
