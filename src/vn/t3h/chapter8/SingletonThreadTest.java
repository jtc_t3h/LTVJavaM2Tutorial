package vn.t3h.chapter8;

public class SingletonThreadTest {

	public static void main(String[] args) throws InterruptedException {
		SingletonThread[] thread = new SingletonThread[5];
		for (int i = 0; i < 5; i++) {
			thread[i] = new SingletonThread();
			thread[i].start();
//			thread[i].join();
		}
		System.out.println("Done!");
	}

}
