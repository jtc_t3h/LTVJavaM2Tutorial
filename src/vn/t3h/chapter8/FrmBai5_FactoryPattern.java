package vn.t3h.chapter8;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;

public class FrmBai5_FactoryPattern extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_FactoryPattern frame = new FrmBai5_FactoryPattern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_FactoryPattern() {
		setTitle("Factory Pattern Demo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDrawImage = new JLabel("DRAW IMAGE");
		lblDrawImage.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDrawImage.setBounds(169, 11, 88, 20);
		contentPane.add(lblDrawImage);
		
		JButton btnCircle = new JButton("Circle");
		btnCircle.setBounds(10, 42, 89, 23);
		contentPane.add(btnCircle);
		
		JButton btnSquare = new JButton("Square");
		btnSquare.setBounds(179, 42, 89, 23);
		contentPane.add(btnSquare);
		
		JButton btnRectangle = new JButton("Rectangle");
		btnRectangle.setBounds(335, 42, 89, 23);
		contentPane.add(btnRectangle);
	}

}
