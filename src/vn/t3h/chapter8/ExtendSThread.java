package vn.t3h.chapter8;

import java.util.Scanner;

public class ExtendSThread extends Thread {

	@Override
	public void run() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap N: ");
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			System.out.println("ExtendSThread: " + i);
		}
	}

	
}
