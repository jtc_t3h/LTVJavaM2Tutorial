package vn.t3h.chapter8;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class FrmBai3_ObserverPattern extends JFrame {

	private JPanel contentPane;
	private JTextField txtBalance;
	private JTextField txtTranfer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_ObserverPattern frame = new FrmBai3_ObserverPattern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_ObserverPattern() {
		setTitle("Observer Pattern");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("INFOMATION OF ACCOUNT XXX");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 11, 414, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblBalance = new JLabel("Balance");
		lblBalance.setBounds(67, 45, 74, 14);
		contentPane.add(lblBalance);
		
		txtBalance = new JTextField();
		txtBalance.setBounds(151, 42, 232, 20);
		contentPane.add(txtBalance);
		txtBalance.setColumns(10);
		
		JLabel lblTranfer = new JLabel("Tranfer");
		lblTranfer.setBounds(67, 81, 74, 14);
		contentPane.add(lblTranfer);
		
		txtTranfer = new JTextField();
		txtTranfer.setColumns(10);
		txtTranfer.setBounds(151, 78, 232, 20);
		contentPane.add(txtTranfer);
		
		JButton btnTranfer = new JButton("Tranfer");
		btnTranfer.setBounds(151, 121, 89, 23);
		contentPane.add(btnTranfer);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 165, 414, 110);
		contentPane.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 286, 414, 110);
		contentPane.add(scrollPane_1);
		
		JTextArea textArea_1 = new JTextArea();
		scrollPane_1.setViewportView(textArea_1);
	}

}
