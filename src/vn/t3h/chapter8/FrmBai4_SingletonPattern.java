package vn.t3h.chapter8;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;

public class FrmBai4_SingletonPattern extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_SingletonPattern frame = new FrmBai4_SingletonPattern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_SingletonPattern() {
		setTitle("Singleton Pattern");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 422, 354);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("GỬI TÀI LIỆU THAM KHẢO");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 11, 386, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnTaoGiaoVien = new JButton("Tạo giáo viên");
		btnTaoGiaoVien.setBounds(145, 56, 104, 23);
		contentPane.add(btnTaoGiaoVien);
		
		JLabel lblHinhAnh = new JLabel("");
		lblHinhAnh.setBounds(10, 92, 173, 212);
		contentPane.add(lblHinhAnh);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(192, 90, 204, 212);
		contentPane.add(scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
	}
}
