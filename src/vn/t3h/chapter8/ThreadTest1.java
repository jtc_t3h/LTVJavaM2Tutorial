package vn.t3h.chapter8;

public class ThreadTest1 {

	public static void main(String[] args) throws InterruptedException {

//		ExtendSThread thread = new ExtendSThread();
//		thread.start();
		
		RunableImpl runableImpl = new RunableImpl();
		Thread thread2 = new Thread(runableImpl);
		thread2.start();
		thread2.setPriority(10);
		
		Runnable runnable = () -> {
			// in ra 100 so tu nhien dau tien
			for (int i = 0; i < 100000; i++) {
				System.out.println("LambdaThread: " + i);
			}
		};
		Thread thread3 = new Thread(runnable);
		thread3.start();
//		thread3.setPriority(10);
		
		
		Thread.sleep(500);
		System.out.println("Main thread: finished!");
//		System.out.println(thread.getName());
		thread2.start();
	}

}
