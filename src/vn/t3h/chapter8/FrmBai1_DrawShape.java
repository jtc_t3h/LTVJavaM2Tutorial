package vn.t3h.chapter8;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTextField;

public class FrmBai1_DrawShape extends JFrame {

	private JPanel contentPane;
	private JTextField txtShape;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_DrawShape frame = new FrmBai1_DrawShape();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_DrawShape() {
		setTitle("Draw Shape");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 227);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblShape = new JLabel("SHAPE");
		lblShape.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblShape.setBounds(21, 14, 76, 17);
		contentPane.add(lblShape);
		
		JLabel lblBorder = new JLabel("BORDER");
		lblBorder.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBorder.setBounds(262, 14, 76, 17);
		contentPane.add(lblBorder);
		
		JRadioButton rdbtnCircle = new JRadioButton("Circle");
		rdbtnCircle.setBounds(21, 42, 109, 23);
		contentPane.add(rdbtnCircle);
		
		JRadioButton rdbtnRectangle = new JRadioButton("Rectangle");
		rdbtnRectangle.setBounds(21, 77, 109, 23);
		contentPane.add(rdbtnRectangle);
		
		JRadioButton rdbtnRedBorder = new JRadioButton("Red Border");
		rdbtnRedBorder.setBounds(262, 42, 109, 23);
		contentPane.add(rdbtnRedBorder);
		
		JButton btnDrawShape = new JButton("Draw shape");
		btnDrawShape.setBounds(161, 109, 102, 23);
		contentPane.add(btnDrawShape);
		
		txtShape = new JTextField();
		txtShape.setBounds(11, 150, 413, 20);
		contentPane.add(txtShape);
		txtShape.setColumns(10);
	}

}
