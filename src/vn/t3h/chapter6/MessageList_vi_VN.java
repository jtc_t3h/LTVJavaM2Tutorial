package vn.t3h.chapter6;

import java.util.ListResourceBundle;

public class MessageList_vi_VN extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		return new Object[][] {{"username", "Tai Khoan"},{"password", "Mat khau"}};
	}

}
