package vn.t3h.chapter6;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleDemo {

	public static void main(String[] args) {
		
		Locale locale = new Locale("vi", "US");
		ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.chapter6.message", locale);
		
		System.out.println(bundle.getString("username"));
		System.out.println(bundle.getString("password"));
		
		System.out.println("Done!");
	}

}
