package vn.t3h.chapter6;

import java.util.regex.Pattern;

public class RegexDemo2 {

	public static void main(String[] args) {
		Pattern p = Pattern.compile("[,.]");
		
		for (String s: p.split("abc,ab.def")) {
			System.out.println(s);
		}
				
	}

}
