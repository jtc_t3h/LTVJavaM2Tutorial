package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class FrmBai5_TimVeMayBay extends JFrame {

	private JPanel contentPane;
	private JTextField txtNgayDi;
	private JTextField txtNgayVe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_TimVeMayBay frame = new FrmBai5_TimVeMayBay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_TimVeMayBay() {
		setTitle("Tìm vé máy bay");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 428);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnViet = new JButton("Việt");
		btnViet.setBounds(10, 11, 62, 23);
		contentPane.add(btnViet);
		
		JButton btnAnh = new JButton("Anh");
		btnAnh.setBounds(82, 11, 62, 23);
		contentPane.add(btnAnh);
		
		JLabel lblTmVMay = new JLabel("Tìm vé may bay");
		lblTmVMay.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTmVMay.setHorizontalAlignment(SwingConstants.CENTER);
		lblTmVMay.setBounds(68, 57, 295, 28);
		contentPane.add(lblTmVMay);
		
		JLabel lblLoiV = new JLabel("Loại vé");
		lblLoiV.setBounds(10, 96, 62, 14);
		contentPane.add(lblLoiV);
		
		JRadioButton rdbMotChieu = new JRadioButton("Một chiều");
		rdbMotChieu.setBounds(107, 92, 109, 23);
		contentPane.add(rdbMotChieu);
		
		JRadioButton rdbKhuHoi = new JRadioButton("Khứ hồi");
		rdbKhuHoi.setBounds(254, 92, 109, 23);
		contentPane.add(rdbKhuHoi);
		
		JLabel lblimKhiHnh = new JLabel("Điểm khởi hành");
		lblimKhiHnh.setBounds(10, 132, 134, 14);
		contentPane.add(lblimKhiHnh);
		
		JComboBox cbbDiemKhoiHanh = new JComboBox();
		cbbDiemKhoiHanh.setBounds(154, 129, 270, 20);
		contentPane.add(cbbDiemKhoiHanh);
		
		JLabel lblimn = new JLabel("Điểm đến");
		lblimn.setBounds(10, 160, 134, 14);
		contentPane.add(lblimn);
		
		JComboBox cbbDiemDen = new JComboBox();
		cbbDiemDen.setBounds(154, 157, 270, 20);
		contentPane.add(cbbDiemDen);
		
		JLabel lblNgyi = new JLabel("Ngày đi");
		lblNgyi.setBounds(10, 190, 134, 14);
		contentPane.add(lblNgyi);
		
		txtNgayDi = new JTextField();
		txtNgayDi.setBounds(154, 188, 191, 20);
		contentPane.add(txtNgayDi);
		txtNgayDi.setColumns(10);
		
		txtNgayVe = new JTextField();
		txtNgayVe.setColumns(10);
		txtNgayVe.setBounds(154, 215, 191, 20);
		contentPane.add(txtNgayVe);
		
		JLabel lblNgyV = new JLabel("Ngày về");
		lblNgyV.setBounds(10, 217, 134, 14);
		contentPane.add(lblNgyV);
		
		JLabel lblNgiLn = new JLabel("Người lớn");
		lblNgiLn.setBounds(10, 250, 134, 14);
		contentPane.add(lblNgiLn);
		
		JComboBox cbbNguoiLon = new JComboBox();
		cbbNguoiLon.setBounds(154, 247, 69, 20);
		contentPane.add(cbbNguoiLon);
		
		JLabel lblTrEm = new JLabel("Trẻ em");
		lblTrEm.setBounds(10, 278, 134, 14);
		contentPane.add(lblTrEm);
		
		JComboBox cbbTreEm = new JComboBox();
		cbbTreEm.setBounds(154, 275, 69, 20);
		contentPane.add(cbbTreEm);
		
		JLabel lblEmB = new JLabel("Em bé");
		lblEmB.setBounds(10, 306, 134, 14);
		contentPane.add(lblEmB);
		
		JComboBox cbbEmBe = new JComboBox();
		cbbEmBe.setBounds(154, 303, 69, 20);
		contentPane.add(cbbEmBe);
		
		JButton btnTimVe = new JButton("Tìm vé");
		btnTimVe.setBounds(154, 353, 89, 23);
		contentPane.add(btnTimVe);
	}

}
