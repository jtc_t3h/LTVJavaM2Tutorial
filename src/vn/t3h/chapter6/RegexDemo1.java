package vn.t3h.chapter6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo1 {

	public static void main(String[] args) {
		String sPattern = "([01]?[0-9]|2[0-3]):[012345]?[0-9]"; // chuoi trong Java
		Pattern p = Pattern.compile(sPattern);
		
		String input = "1:60";
		Matcher matcher = p.matcher(input);
		if (matcher.matches()) {
			System.out.println("chuoi hop le.");
		} else {
			System.out.println("chuoi khong hop le.");
		}
	}

}
