package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Locale;
import java.util.ResourceBundle;

public class FrmBai4_ResourceBundle extends JFrame {

	private JPanel contentPane;
	private ButtonGroup langGroup = new ButtonGroup();
	private JRadioButton rdbVietnamese;
	private JRadioButton rdbEnglish;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_ResourceBundle frame = new FrmBai4_ResourceBundle();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_ResourceBundle() {
		setTitle("Resource Bundle cho ngôn ngữ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLanguage = new JLabel("Select language");
		lblLanguage.setBounds(10, 11, 138, 14);
		contentPane.add(lblLanguage);
		
		rdbVietnamese = new JRadioButton("Vietnamese");
		rdbVietnamese.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Locale locale = new Locale("vi", "VN");
				ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.chapter6.message", locale);
				
				lblLanguage.setText(bundle.getString("label.language"));
				rdbVietnamese.setText(bundle.getString("label.vi"));
				rdbEnglish.setText(bundle.getString("label.en"));
			}
		});
		rdbVietnamese.setBounds(154, 7, 109, 23);
		contentPane.add(rdbVietnamese);
		langGroup.add(rdbVietnamese);
		
		rdbEnglish = new JRadioButton("English");
		rdbEnglish.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Locale locale = new Locale("en", "US");
				ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.chapter6.message", locale);
				
				lblLanguage.setText(bundle.getString("label.language"));
				rdbVietnamese.setText(bundle.getString("label.vi"));
				rdbEnglish.setText(bundle.getString("label.en"));
				
				
			}
		});
		rdbEnglish.setSelected(true);
		rdbEnglish.setBounds(279, 7, 109, 23);
		contentPane.add(rdbEnglish);
		langGroup.add(rdbEnglish);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 414, 214);
		contentPane.add(scrollPane);
		
		
		JList list = new JList();
		scrollPane.setViewportView(list);
	}
}
