package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class FrmBai1_UsernamFormat extends JFrame {

	private JPanel contentPane;
	private JTextField txtPattern;
	private JTextField txtUsername;
	private JTextField txtKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_UsernamFormat frame = new FrmBai1_UsernamFormat();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_UsernamFormat() {
		setTitle("Kiểm tra username theo mẫu định nghĩa trước");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 191);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMunhDng = new JLabel("Mẫu định dạng username");
		lblMunhDng.setBounds(10, 11, 138, 14);
		contentPane.add(lblMunhDng);
		
		txtPattern = new JTextField();
		txtPattern.setBounds(157, 8, 267, 20);
		contentPane.add(txtPattern);
		txtPattern.setColumns(10);
		
		JLabel lblUsernameCnKim = new JLabel("Username cần kiểm tra");
		lblUsernameCnKim.setBounds(10, 40, 138, 14);
		contentPane.add(lblUsernameCnKim);
		
		txtUsername = new JTextField();
		txtUsername.setColumns(10);
		txtUsername.setBounds(157, 37, 267, 20);
		contentPane.add(txtUsername);
		
		JButton btnKiemTra = new JButton("Kiểm tra");
		btnKiemTra.setBounds(157, 68, 89, 23);
		contentPane.add(btnKiemTra);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(10, 123, 138, 14);
		contentPane.add(lblKtQu);
		
		txtKetQua = new JTextField();
		txtKetQua.setColumns(10);
		txtKetQua.setBounds(157, 120, 267, 20);
		contentPane.add(txtKetQua);
	}

}
