package vn.t3h.chapter6;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo_UserNameFormat {

	public static void main(String[] args) {
		String regex = "[a-z0-9_-]{6,20}";
		Pattern pattern = Pattern.compile(regex);
		
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("Chuoi can kiem tra:");
			String input = sc.nextLine();
			
			if ("q".equalsIgnoreCase(input)) {
				break;
			}
			Matcher match = pattern.matcher(input);
			if (match.matches()) {
				System.out.println("username hop le.");
			} else {
				System.out.println("username khong hop le");
			}
		}
	}

}
