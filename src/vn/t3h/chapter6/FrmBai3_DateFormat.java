package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class FrmBai3_DateFormat extends JFrame {

	private JPanel contentPane;
	private JTextField txtPattern;
	private JTextField txtNgay;
	private JTextField txtKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_DateFormat frame = new FrmBai3_DateFormat();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_DateFormat() {
		setTitle("Kiểm tra ngày tháng theo mẫu định nghĩa trước");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 191);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMunhDng = new JLabel("Mẫu định dạng ngày");
		lblMunhDng.setBounds(10, 11, 138, 14);
		contentPane.add(lblMunhDng);
		
		txtPattern = new JTextField();
		txtPattern.setBounds(157, 8, 267, 20);
		contentPane.add(txtPattern);
		txtPattern.setColumns(10);
		
		JLabel lblUsernameCnKim = new JLabel("Ngày kiểm tra");
		lblUsernameCnKim.setBounds(10, 40, 138, 14);
		contentPane.add(lblUsernameCnKim);
		
		txtNgay = new JTextField();
		txtNgay.setColumns(10);
		txtNgay.setBounds(157, 37, 267, 20);
		contentPane.add(txtNgay);
		
		JButton btnKiemTra = new JButton("Kiểm tra");
		btnKiemTra.setBounds(157, 68, 89, 23);
		contentPane.add(btnKiemTra);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(10, 123, 138, 14);
		contentPane.add(lblKtQu);
		
		txtKetQua = new JTextField();
		txtKetQua.setColumns(10);
		txtKetQua.setBounds(157, 120, 267, 20);
		contentPane.add(txtKetQua);
	}

}
