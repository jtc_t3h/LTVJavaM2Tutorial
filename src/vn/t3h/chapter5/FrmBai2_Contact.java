package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class FrmBai2_Contact extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDTDD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Contact frame = new FrmBai2_Contact();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Contact() {
		setTitle("Thêm mới liên hệ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 570, 263);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHTn = new JLabel("H\u1ECD t\u00EAn");
		lblHTn.setBounds(10, 11, 59, 14);
		contentPane.add(lblHTn);

		JLabel lbltd = new JLabel("DTDD");
		lbltd.setBounds(10, 36, 46, 14);
		contentPane.add(lbltd);

		JLabel lblHnhnh = new JLabel("H\u00ECnh \u1EA3nh");
		lblHnhnh.setBounds(10, 61, 59, 14);
		contentPane.add(lblHnhnh);

		JTextField txtHinhAnh = new JTextField();
		txtHinhAnh.setBounds(79, 58, 216, 20);
		contentPane.add(txtHinhAnh);
		txtHinhAnh.setColumns(10);

		JLabel lblHinhAnh = new JLabel("");
		lblHinhAnh.setBounds(378, 11, 166, 201);
		contentPane.add(lblHinhAnh);

		JButton btnBrowser = new JButton("...");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JFileChooser jfc = new JFileChooser();
				int returnVal = jfc.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File fileSelected = jfc.getSelectedFile();

					txtHinhAnh.setText(fileSelected.getAbsolutePath());
					lblHinhAnh.setIcon(new ImageIcon(fileSelected.getAbsolutePath()));
				}

			}
		});
		btnBrowser.setBounds(301, 57, 46, 23);
		contentPane.add(btnBrowser);

		txtHoTen = new JTextField();
		txtHoTen.setBounds(79, 8, 216, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);

		txtDTDD = new JTextField();
		txtDTDD.setBounds(79, 33, 216, 20);
		contentPane.add(txtDTDD);
		txtDTDD.setColumns(10);

		JButton btnThemMoi = new JButton("Th\u00EAm m\u1EDBi");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String hoTen = txtHoTen.getText().trim();
				String dtdd = txtDTDD.getText().trim();
				String hinhAnh = txtHinhAnh.getText().trim();
				Bai2_Contact contact = new Bai2_Contact(hoTen, dtdd, hinhAnh);

				Connection connection = null;
				PreparedStatement statement = null;
				try {
					String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
					connection = DriverManager.getConnection(url, "root", "");
					
					String sql = "INSERT INTO lienhe(hoten, dtdd, hinhanh) VALUES (?,?,?)";
					statement = connection.prepareStatement(sql);
					statement.setString(1, contact.getHoTen());
					statement.setString(2, contact.getDtdd());
					statement.setString(3, contact.getHinhAnh());
					
					int rowUpdate = statement.executeUpdate();
					if (rowUpdate == 1){
						JOptionPane.showMessageDialog(null, "Đã thêm liên hệ thành công!");
					} else {
						JOptionPane.showMessageDialog(null, "Thêm mới liên hệ lỗi!");
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if (statement != null)
							statement.close();
						if (connection != null)
							connection.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		});
		btnThemMoi.setBounds(79, 187, 105, 23);
		contentPane.add(btnThemMoi);
	}

}
