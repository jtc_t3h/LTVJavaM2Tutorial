package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class FrmBai1_DangNhap extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_DangNhap frame = new FrmBai1_DangNhap();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_DangNhap() {
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 152);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Username");
		lblNewLabel.setBounds(10, 22, 66, 14);
		contentPane.add(lblNewLabel);

		txtUsername = new JTextField();
		txtUsername.setBounds(86, 19, 174, 20);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 49, 66, 14);
		contentPane.add(lblPassword);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Connection con = null;
				Statement st = null;
				ResultSet rs = null;
				try {
					String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
					con = DriverManager.getConnection(url, "root", "");

					st = con.createStatement();
					rs = st.executeQuery("select * from user where username = '"
							+ txtUsername.getText().trim() + "' and password = '" + String.valueOf(txtPassword.getPassword()) + "' ");
					
					int rowCount = 0;
					while (rs.next()) {
						rowCount += 1;
					}
					
					if (rowCount == 1) {
						JOptionPane.showMessageDialog(null, "Login Successful!");
					} else {
						JOptionPane.showMessageDialog(null, "Login Fail!");
					}
					
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					try {
						if (rs != null) rs.close();
						if (st != null) st.close();
						if (con != null) con.close();
					} catch (Exception exc) {
						exc.printStackTrace();
					}
				}
			}
		});
		btnLogin.setBounds(86, 84, 89, 23);
		contentPane.add(btnLogin);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(86, 46, 174, 20);
		contentPane.add(txtPassword);
	}
}
