package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai5_CapNhatSach extends JFrame {

	private JPanel contentPane;
	private JTextField txtTenSach;
	private JTextField txtTacGia;
	private JTextField txtNXB;
	private JTextField txtGiaBia;
	private JTable table;
	private int idSelected;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_CapNhatSach frame = new FrmBai5_CapNhatSach();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_CapNhatSach() {
		setTitle("Cập nhật sách");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 474);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTnSch = new JLabel("Tên sách");
		lblTnSch.setBounds(10, 11, 90, 14);
		contentPane.add(lblTnSch);
		
		txtTenSach = new JTextField();
		txtTenSach.setBounds(110, 8, 314, 20);
		contentPane.add(txtTenSach);
		txtTenSach.setColumns(10);
		
		JLabel lblTcGi = new JLabel("Tác giả");
		lblTcGi.setBounds(10, 39, 90, 14);
		contentPane.add(lblTcGi);
		
		txtTacGia = new JTextField();
		txtTacGia.setColumns(10);
		txtTacGia.setBounds(110, 36, 314, 20);
		contentPane.add(txtTacGia);
		
		JLabel lblNxb = new JLabel("NXB");
		lblNxb.setBounds(10, 67, 90, 14);
		contentPane.add(lblNxb);
		
		txtNXB = new JTextField();
		txtNXB.setColumns(10);
		txtNXB.setBounds(110, 64, 314, 20);
		contentPane.add(txtNXB);
		
		JLabel lblGiBa = new JLabel("Giá bìa");
		lblGiBa.setBounds(10, 95, 90, 14);
		contentPane.add(lblGiBa);
		
		txtGiaBia = new JTextField();
		txtGiaBia.setColumns(10);
		txtGiaBia.setBounds(110, 92, 314, 20);
		contentPane.add(txtGiaBia);
		
		JButton btnCapNhat = new JButton("Cập nhật sách");
		btnCapNhat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(rootPane, "Bạn có chắn chắc muốn cập nhât dữ liệu không?", "Xác Nhận", 
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					
					String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
					String username = "root";
					String password = "";
					try (Connection con = DriverManager.getConnection(url, username, password);
							PreparedStatement pr = con.prepareStatement("update sach set tensach = ?, tacgia = ?, nxb = ?, giabia = ? where id = ?")){
						
						pr.setString(1, txtTenSach.getText().trim());
						pr.setString(2, txtTacGia.getText().trim());
						pr.setString(3, txtNXB.getText().trim());
						pr.setLong(4, Long.parseLong(txtGiaBia.getText().trim()));
						pr.setInt(5, idSelected);
						
						if (pr.executeUpdate() == 1){
							initDataFromCSDL();
							
							JOptionPane.showMessageDialog(null, "Cập nhật thành công.");
						} else {
							JOptionPane.showMessageDialog(null, "Cập nhật không thành công.");
						}
					} catch (SQLException e2) {
						JOptionPane.showMessageDialog(null, "Đã có lỗi xãy ra.");
						e2.printStackTrace();
					}
				}
			}
		});
		btnCapNhat.setBounds(110, 123, 120, 23);
		contentPane.add(btnCapNhat);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 162, 414, 262);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				idSelected = (int) table.getValueAt(table.getSelectedRow(), 0);
				txtTenSach.setText((String) table.getValueAt(table.getSelectedRow(), 1));
				txtTacGia.setText((String) table.getValueAt(table.getSelectedRow(), 2));
				txtNXB.setText((String) table.getValueAt(table.getSelectedRow(), 3));
				txtGiaBia.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), 4)));
			}
		});
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"ID", "T\u00EAn s\u00E1ch", "T\u00E1c gi\u1EA3", "NXB", "Gi\u00E1 b\u00ECa"
			}
		));
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		scrollPane.setViewportView(table);
		
		initDataFromCSDL();
	}

	private void initDataFromCSDL() {
		String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
		String username = "root";
		String password = "";
		
		try (Connection con = DriverManager.getConnection(url, username, password);
				Statement pr = con.createStatement();
				ResultSet rs = pr.executeQuery("select * from sach")){
			
			if (rs != null){
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				while (rs.next()){
					model.addRow(new Object[]{rs.getInt(1),rs.getString("TenSach"), rs.getString("TacGia"), rs.getString("NXB"), rs.getLong("GiaBia")});
				}
			} else {
				JOptionPane.showMessageDialog(null, "Chưa có dữ liệu.");
			}
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(null, "Đã có lỗi xãy ra.");
			e2.printStackTrace();
		} 
	}

}
