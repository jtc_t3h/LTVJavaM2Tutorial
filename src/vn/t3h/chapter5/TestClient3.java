package vn.t3h.chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestClient3 {

	public static void main(String[] args) throws SQLException {
		Connection con = null;
		PreparedStatement pr = null;
		int rowNum = 0;
		try {
			
			String url = "jdbc:mysql://127.0.0.1/phan_cong_nhan_vien_1_1_nam";
			
			con = DriverManager.getConnection(url, "root", "");
//			con.setAutoCommit(false);
			
			String sql = "insert into nguoi_dung (id, ten) value (?,?)";
			pr = con.prepareStatement(sql);
			
			pr.setInt(1, 151);
			pr.setString(2, "NV_151");
			rowNum += pr.executeUpdate();
			
			pr.setInt(1, 150);
			pr.setString(2, "NV_150");
			rowNum += pr.executeUpdate();
						
			System.out.println(rowNum);
//			con.commit();
//			con.setAutoCommit(true);
		} catch (Exception e) {
//			if (con != null) con.rollback();
			e.printStackTrace();
		} finally {
			if (pr != null) pr.close();
			if (con != null) con.close();
		}
	}

}
