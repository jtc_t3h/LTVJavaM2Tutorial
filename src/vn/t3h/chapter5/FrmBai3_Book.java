package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class FrmBai3_Book extends JFrame {

	private JPanel contentPane;
	private JTextField txtTenSach;
	private JTextField txtTacGia;
	private JTextField txtNXB;
	private JTextField txtGiaBia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_Book frame = new FrmBai3_Book();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_Book() {
		setTitle("Thêm sách");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 333, 217);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTnSch = new JLabel("Tên sách");
		lblTnSch.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTnSch.setBounds(10, 11, 65, 14);
		contentPane.add(lblTnSch);
		
		txtTenSach = new JTextField();
		txtTenSach.setBounds(85, 8, 206, 20);
		contentPane.add(txtTenSach);
		txtTenSach.setColumns(10);
		
		JLabel lblTcGi = new JLabel("Tác giả");
		lblTcGi.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTcGi.setBounds(10, 39, 65, 14);
		contentPane.add(lblTcGi);
		
		txtTacGia = new JTextField();
		txtTacGia.setColumns(10);
		txtTacGia.setBounds(85, 36, 206, 20);
		contentPane.add(txtTacGia);
		
		JLabel lblNxb = new JLabel("NXB");
		lblNxb.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNxb.setBounds(10, 67, 65, 14);
		contentPane.add(lblNxb);
		
		txtNXB = new JTextField();
		txtNXB.setColumns(10);
		txtNXB.setBounds(85, 64, 206, 20);
		contentPane.add(txtNXB);
		
		JLabel lblGiBa = new JLabel("Giá bìa");
		lblGiBa.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGiBa.setBounds(10, 95, 65, 14);
		contentPane.add(lblGiBa);
		
		txtGiaBia = new JTextField();
		txtGiaBia.setColumns(10);
		txtGiaBia.setBounds(85, 92, 206, 20);
		contentPane.add(txtGiaBia);
		
		JButton btnThemSachMoi = new JButton("Thêm sách mới");
		btnThemSachMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String tenSach = txtTenSach.getText().trim();
				String tacGia = txtTacGia.getText().trim();
				String nxb = txtNXB.getText().trim();
				long giaBia = Long.parseLong(txtGiaBia.getText().trim());
				
				Bai3_Book book = new Bai3_Book(tenSach, tacGia, nxb, giaBia);
				
				String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
				String username = "root";
				String password = "";
				try (Connection con = DriverManager.getConnection(url, username, password);
						PreparedStatement pr = con.prepareStatement("insert into sach(tensach,tacgia,nxb,giabia) values (?,?,?,?)")){
					
					pr.setString(1, book.getTenSach());
					pr.setString(2, book.getTacGia());
					pr.setString(3, book.getNxb());
					pr.setLong(4, book.getGiaBia());
					
					int retValue = pr.executeUpdate();
					if (retValue == 1){
						JOptionPane.showMessageDialog(null, "Thêm sách mới thành công.");
					} else {
						JOptionPane.showMessageDialog(null, "Thêm sách mới thất bại.");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Đã có lỗi xãy ra.");
					e2.printStackTrace();
				} 
			}
		});
		btnThemSachMoi.setBounds(53, 134, 120, 23);
		contentPane.add(btnThemSachMoi);
		
		JButton btnTiepTuc = new JButton("Tiếp tục");
		btnTiepTuc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtTenSach.setText("");
				txtTacGia.setText("");
				txtNXB.setText("");
				txtGiaBia.setText("");
			}
		});
		btnTiepTuc.setBounds(176, 134, 89, 23);
		contentPane.add(btnTiepTuc);
	}

}
