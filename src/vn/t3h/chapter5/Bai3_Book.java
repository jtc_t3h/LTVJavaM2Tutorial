package vn.t3h.chapter5;

public class Bai3_Book {

	private String tenSach;
	private String tacGia;
	private String nxb;
	private long giaBia;

	public Bai3_Book(String tenSach, String tacGia, String nxb, long giaBia) {
		this.tenSach = tenSach;
		this.tacGia = tacGia;
		this.nxb = nxb;
		this.giaBia = giaBia;
	}

	public String getTenSach() {
		return tenSach;
	}

	public String getTacGia() {
		return tacGia;
	}

	public String getNxb() {
		return nxb;
	}

	public long getGiaBia() {
		return giaBia;
	}

	public void setTenSach(String tenSach) {
		this.tenSach = tenSach;
	}

	public void setTacGia(String tacGia) {
		this.tacGia = tacGia;
	}

	public void setNxb(String nxb) {
		this.nxb = nxb;
	}

	public void setGiaBia(long giaBia) {
		this.giaBia = giaBia;
	}

}
