package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class FrmBai4_SearchBook extends JFrame {

	private JPanel contentPane;
	private JTextField txtTenSach;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_SearchBook frame = new FrmBai4_SearchBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_SearchBook() {
		setTitle("Tìm kiếm sách - xóa sách");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTnSch = new JLabel("Tên sách");
		lblTnSch.setBounds(10, 11, 62, 14);
		contentPane.add(lblTnSch);
		
		txtTenSach = new JTextField();
		txtTenSach.setBounds(82, 8, 342, 20);
		contentPane.add(txtTenSach);
		txtTenSach.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 74, 414, 176);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"ID", "T\u00EAn s\u00E1ch", "T\u00E1c gi\u1EA3", "NXB", "Gi\u00E1 b\u00ECa"
			}
		));
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		scrollPane.setViewportView(table);
		
		JButton btnTimSach = new JButton("Tìm sách");
		btnTimSach.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String tenSach = txtTenSach.getText().trim().toUpperCase();
				String sql = "";
				if (tenSach.isEmpty()){
					sql = "select * from sach";
				} else {
					sql = "select * from sach where upper(TenSach) like '%" + tenSach + "%'";
				}
				
				String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
				String username = "root";
				String password = "";
				
				try (Connection con = DriverManager.getConnection(url, username, password);
						Statement pr = con.createStatement();
						ResultSet rs = pr.executeQuery(sql)){
					
					if (rs != null){
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.setRowCount(0);
						while (rs.next()){
							model.addRow(new Object[]{rs.getInt(1),rs.getString("TenSach"), rs.getString("TacGia"), rs.getString("NXB"), rs.getLong("GiaBia")});
						}
					} else {
						JOptionPane.showMessageDialog(null, "Không tìm thấy dữ liệu.");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Đã có lỗi xãy ra.");
					e2.printStackTrace();
				} 
			}
		});
		btnTimSach.setBounds(82, 40, 89, 23);
		contentPane.add(btnTimSach);
		
		JButton btnXoaSach = new JButton("Xóa sách");
		btnXoaSach.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(rootPane, "Bạn có chắn chắc muốn xóa dữ không?", "Xác Nhận", 
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					
					String url = "jdbc:mysql://127.0.0.1/qllienhe?useUnicode=true&characterEncoding=utf8&useSSL=false";
					String username = "root";
					String password = "";
					try (Connection con = DriverManager.getConnection(url, username, password);
							PreparedStatement pr = con.prepareStatement("delete from sach where id = ?")){
						pr.setInt(1, (int) table.getValueAt(table.getSelectedRow(), 0));
						if (pr.executeUpdate() == 1){
							DefaultTableModel model = (DefaultTableModel) table.getModel();
							model.removeRow(table.getSelectedRow());
							
							JOptionPane.showMessageDialog(null, "Xóa thành công.");
						} else {
							JOptionPane.showMessageDialog(null, "Xóa không thành công.");
						}
					} catch (SQLException e2) {
						JOptionPane.showMessageDialog(null, "Đã có lỗi xãy ra.");
						e2.printStackTrace();
					}
				}
			}
		});
		btnXoaSach.setBounds(238, 40, 89, 23);
		contentPane.add(btnXoaSach);
	}
}
