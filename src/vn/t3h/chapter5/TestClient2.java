package vn.t3h.chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class TestClient2 {

	public static void main(String[] args) throws SQLException {

		Connection con = null;
		PreparedStatement pr = null;
		try {
			
			String url = "jdbc:mysql://127.0.0.1/phan_cong_nhan_vien_1_1_nam";
			
			con = DriverManager.getConnection(url, "root", "");
			
			String sql = "insert into nguoi_dung (id, ten) value (?,?)";
			pr = con.prepareStatement(sql);
			
			pr.setInt(1, 148);
			pr.setString(2, "NV_148");
			int rowNum = pr.executeUpdate();
			
			System.out.println(rowNum);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pr != null) pr.close();
			if (con != null) con.close();
		}
	}

}
