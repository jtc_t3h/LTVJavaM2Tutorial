package vn.t3h.chapter5;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.Driver;

public class TestClient {

	public static void main(String[] args) {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		// Buoc 2: tao chuoi ket noi
		String url = "jdbc:mysql://127.0.0.1/phan_cong_nhan_vien_1_1_nam";
		
		// Buoc 2: Tao ket noi
		try {
			Connection con = DriverManager.getConnection(url, "root", "");
			
		    DatabaseMetaData metaData = con.getMetaData();
		    System.out.println(metaData.getDatabaseProductName());
		    System.out.println(metaData.getDatabaseProductVersion());
		    
		    // Buoc 4: Tao doi tuong Statement
		    Statement st = con.createStatement();
		    
		    String sql = "select * from nguoi_dung where id = 151";
		    ResultSet rs = st.executeQuery(sql);
		    
		    // Buoc 5: xu ly ket qua -> xu ly ResultSet
		    int count = 0;
		    while (rs.next()) {
		    	count += 1;
		    	System.out.println( rs.getInt(1) + "	" + rs.getString("Ten"));
		    }
			System.out.println("So luong row = " + count);
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Done!");
	}

}
