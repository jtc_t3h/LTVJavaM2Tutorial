package vn.t3h.chapter5;

import java.io.Serializable;

public class Bai2_Contact implements Serializable {

	private int id;
	private String hoTen;
	private String dtdd;
	private String hinhAnh;

	
	public Bai2_Contact(String hoTen, String dtdd, String hinhAnh) {
		this.hoTen = hoTen;
		this.dtdd = dtdd;
		this.hinhAnh = hinhAnh;
	}

	public int getId() {
		return id;
	}

	public String getHoTen() {
		return hoTen;
	}

	public String getDtdd() {
		return dtdd;
	}

	public String getHinhAnh() {
		return hinhAnh;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public void setDtdd(String dtdd) {
		this.dtdd = dtdd;
	}

	public void setHinhAnh(String hinhAnh) {
		this.hinhAnh = hinhAnh;
	}

}
